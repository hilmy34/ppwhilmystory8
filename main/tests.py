from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import search
import json
import requests

class TestStory8(TestCase):
    def test_url_ada_apa_nggak(self):
        response = Client().get('')
        self.assertEquals(200, response.status_code)

    def test_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'main/story8.html')

    def test_view_search(self):
        response = Client().get("/buku/web")
        self.assertContains(response, "web", status_code=200)
    