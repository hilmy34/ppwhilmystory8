from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

def home(request):
    return render(request, 'main/story8.html')

def search(request, key):
    result= requests.get("https://www.googleapis.com/books/v1/volumes?q=" + key).json()
    return JsonResponse(result)

